#!/usr/bin/env stack
-- stack --install-ghc --resolver lts-5.13 runghc --package http-client-tls --package aeson
{-# LANGUAGE OverloadedStrings #-}
import           Data.Aeson
import           Data.Char
import qualified Data.ByteString.Char8 as S8
import qualified Data.ByteString.Lazy.Char8 as L8
import           Network.HTTP.Client
import           Network.HTTP.Client.TLS
import           Network.HTTP.Types.Status
import           System.Environment

url = "http://tictactoe.homedir.eu/game/"

data Move = Move Char Int Int
instance ToJSON Move where
   toJSON (Move player x y) = object
        [ "x" .= x,
          "y"  .= y,
          "v" .= player
        ]

charArrToString :: [Char] -> String
charArrToString charArr =
    let
        string = if (length charArr > 0)
            then charToString (charArr !! 0) ++ charArrToString (tail charArr)
            else ""

    in
        string

charToString :: Char -> String
charToString c = [c]

intToString :: Int -> String
intToString i = charToString (intToDigit i)

printMove :: Move -> IO()
printMove move = do
    print (serializeMove 0 move)

printTable :: [Move] -> IO()
printTable moves = do
    print (serializeTable moves)

addNewMove :: [Move] -> Move -> [Move]
addNewMove [] move = [move]
addNewMove moves move =
    let
        newMoves = move : (reverse moves)
    in
        reverse newMoves

removeLastMove :: [Move] -> [Move]
removeLastMove [] = []
removeLastMove moves = reverse (tail (reverse moves))

serializeMove :: Int -> Move -> String
serializeMove index (Move player x y) = "\"" ++ intToString index ++ "\": {\"x\": " ++ intToString x ++ ", \"y\": " ++ intToString y ++ ", \"v\": \"" ++ charToString player ++ "\"}"

concatSerializedMoves :: [Move] -> Int -> String
concatSerializedMoves [] index = ""
concatSerializedMoves moves index =
    let
        serialized = if (length moves > 1)
            then serializeMove index (head moves) ++ ", " ++ concatSerializedMoves (tail moves) (index + 1)
            else serializeMove index (head moves)
    in serialized

serializeTable :: [Move] -> String
serializeTable [] = "{}"
serializeTable moves = "{" ++ (concatSerializedMoves moves 0) ++ "}"

trim :: [Char] -> [Char]
trim [] = []
trim (' ':xs) = trim xs
trim (x:xs) =
  let
    rest = trim xs
  in
    x:rest

getPlayerOfMove :: Move -> Char
getPlayerOfMove (Move player x y) = player

getCellOfMove :: Move -> (Int, Int)
getCellOfMove (Move player x y) = (x, y)

isEmptyCell :: [Move] -> (Int, Int) -> Bool
isEmptyCell moves (xCord, yCord) = (0 == (length $ filter (\(Move player x y) -> (xCord == x) && (yCord == y)) moves))

getBlock :: [Move] -> Char -> Int -> [Move]
getBlock moves typ index =
    let
        block = if (typ == 'r')
            then filter (\(Move player x y) -> (x == index)) moves
            else if (typ == 'c')
                then filter (\(Move player x y) -> (y == index)) moves
                else if (index == 0)
                    then filter (\(Move player x y) -> ((x == 0) && (y == 0)) || ((x == 1) && (y == 1)) || ((x == 2) && (y == 2))) moves
                    else filter (\(Move player x y) -> ((x == 0) && (y == 2)) || ((x == 1) && (y == 1)) || ((x == 2) && (y == 0))) moves
    in
        block

twoSameInABlock :: [Move] -> Int -> Char -> Bool
twoSameInABlock moves coord typ =
    let
        exists = 
            (2 == (length $ filter (\(Move player x y) -> (player == 'x')) (getBlock moves typ coord))) ||
            (2 == (length $ filter (\(Move player x y) -> (player == 'o')) (getBlock moves typ coord)))
    in
        exists

getFirstEmptyDiagonalCell :: [Move] -> Int -> (Int, Int)
getFirstEmptyDiagonalCell moves diagonal =
    let
        cell = if (diagonal == 0)
            then
                if (isEmptyCell moves (0, 0))
                    then (0, 0)
                    else if (isEmptyCell moves (1, 1))
                        then (1, 1)
                        else if (isEmptyCell moves (2, 2))
                            then (2, 2)
                            else (-1, -1)
            else
                if (isEmptyCell moves (0, 2))
                    then (0, 2)
                    else if (isEmptyCell moves (1, 1))
                        then (1, 1)
                        else if (isEmptyCell moves (2, 0))
                            then (2, 0)
                            else (-1, -1)
    in
        cell

getFirstEmptyCell :: [Move] -> Int -> Int -> Char -> (Int, Int)
getFirstEmptyCell moves row 3 typ = (-1, -1)
getFirstEmptyCell moves 3 col typ = (-1, -1)
getFirstEmptyCell moves row (-1) typ = (-1, -1)
getFirstEmptyCell moves (-1) col typ = (-1, -1)
getFirstEmptyCell moves row col typ =
    let
        cell = if (isEmptyCell moves (row, col))
            then (row, col)
            else if (typ == 'r')
                then (getFirstEmptyCell moves row (col + 1) typ)
                else if (typ == 'c')
                    then (getFirstEmptyCell moves (row + 1) col typ)
                    else if (typ == 'd')
                        then (getFirstEmptyDiagonalCell moves 0)
                        else if (typ == 'b')
                            then (getFirstEmptyDiagonalCell moves 1)
                            else if (typ == 't')
                                then getFirstEmptyCell moves (if (col == 2) then row + 1 else row) (if (col == 2) then 0 else col + 1) typ
                                else getFirstEmptyCell moves 3 3 typ
    in
        cell

existsEmptyCell :: [Move] -> Int -> Int -> Char -> Bool
existsEmptyCell moves row col typ =
    let
        cell = getFirstEmptyCell moves row col typ
    in
        (fst cell) > -1

getEmptyCorner :: [Move] -> (Int, Int)
getEmptyCorner moves =
    let
        cell = if (isEmptyCell moves (0, 0))
            then (0, 0)
            else if (isEmptyCell moves (0, 2))
                then (0, 2)
                else if (isEmptyCell moves (2, 0))
                    then (2, 0)
                    else if (isEmptyCell moves (2, 2))
                        then (2, 2)
                        else (-1, -1)
    in
        cell

existsEmptyCorner :: [Move] -> Bool
existsEmptyCorner moves =
    let
        cell = getEmptyCorner moves
    in
        (fst cell) > -1

isInCorner :: Move -> Bool
isInCorner move =
    let
        cell = getCellOfMove move
        inCorner = 
            ((0, 0) == (cell)) ||
            ((0, 2) == (cell)) ||
            ((2, 0) == (cell)) ||
            ((2, 2) == (cell))
    in
        inCorner

isInMiddle :: Move -> Bool
isInMiddle move = (1, 1) == (getCellOfMove move)

getOppositeCorner :: Move -> (Int, Int)
getOppositeCorner move =
    let
        cell =
            if ((getCellOfMove move) == (0, 0))
                then (2, 2)
                else if ((getCellOfMove move) == (0, 2))
                    then (2, 0)
                    else if ((getCellOfMove move) == (2, 2))
                        then (0, 0)
                        else if ((getCellOfMove move) == (2, 0))
                            then (0, 2)
                            else (-1, -1)
    in
        cell

getFirstEmptySideCell :: [Move] -> (Int, Int)
getFirstEmptySideCell moves =
    let
        cell = if (isEmptyCell moves (0, 1))
            then (0, 1)
            else if (isEmptyCell moves (1, 0))
                then (1, 0)
                else if (isEmptyCell moves (1, 2))
                    then (1, 2)
                    else if (isEmptyCell moves(2, 1))
                        then (2, 1)
                        else (-1, -1)
    in
        cell

getOccupiedCorners :: [Move] -> [Move]
getOccupiedCorners moves = filter (\(Move player x y) -> ((x == 0) && (y == 0)) || ((x == 0) && (y == 2)) || ((x == 2) && (y == 0)) || ((x == 2) && (y == 2))) moves

isInOppositeCorner :: Move -> Move -> Bool
isInOppositeCorner move1 move2 = ((getOppositeCorner move1) == (getCellOfMove move2))

defendRows :: [Move] -> Int -> (Int, Int)
defendRows moves 3 = (-1, -1)
defendRows moves row =
    let
        cell = if ((twoSameInABlock moves row 'r') && (existsEmptyCell moves row 0 'r'))
            then (getFirstEmptyCell moves row 0 'r')
            else (defendRows moves (row + 1))
    in
        cell

defendCols :: [Move] -> Int -> (Int, Int)
defendCols moves 3 = (-1, -1)
defendCols moves col =
    let
        cell = if ((twoSameInABlock moves col 'c') && (existsEmptyCell moves 0 col 'c'))
            then (getFirstEmptyCell moves 0 col 'c')
            else (defendCols moves (col + 1))
    in
        cell

defendDiags :: [Move] -> Int -> (Int, Int)
defendDiags moves 0 =
    let
        cell = if ((twoSameInABlock moves 0 'd') && (existsEmptyCell moves 0 0 'd'))
            then (getFirstEmptyCell moves 0 0 'd')
            else (-1, -1)
    in
        cell
defendDiags moves 1 =
    let
        cell = if ((twoSameInABlock moves 1 'd') && (existsEmptyCell moves 0 0 'b'))
            then (getFirstEmptyCell moves 0 0 'b')
            else (-1, -1)
    in
        cell

defend :: [Move] -> (Int, Int)
defend moves =
    let
        cell =
            if ((-1, -1) /= (defendRows moves 0))
                then (defendRows moves 0)
                else if ((-1, -1) /= (defendCols moves 0))
                    then (defendCols moves 0)
                    else if ((-1, -1) /= (defendDiags moves 0))
                        then (defendDiags moves 0)
                        else (-1, -1)
    in
        cell

generateWinnerRowMove :: [Move] -> Char -> Int -> (Int, Int)
generateWinnerRowMove moves player 3 = (-1, -1)
generateWinnerRowMove moves player row = 
    let
        cell =
            if ((2 == length (getBlock moves 'r' row)) && (2 == (length $ filter (\(Move pl x y) -> (player == pl)) (getBlock moves 'r' row))))
                then getFirstEmptyCell moves row 0 'r'
                else generateWinnerRowMove moves player (row + 1)
    in
        cell

generateWinnerColMove :: [Move] -> Char -> Int -> (Int, Int)
generateWinnerColMove moves player 3 = (-1, -1)
generateWinnerColMove moves player col = 
    let
        cell =
            if ((2 == length (getBlock moves 'c' col)) && (2 == (length $ filter (\(Move pl x y) -> (player == pl)) (getBlock moves 'c' col))))
                then getFirstEmptyCell moves 0 col 'c'
                else generateWinnerRowMove moves player (col + 1)
    in
        cell

generateWinnerDiagMove :: [Move] -> Char -> Int -> (Int, Int)
generateWinnerDiagMove moves player 2 = (-1, -1)
generateWinnerDiagMove moves player diag =
    let
        cell =
            if ((2 == length (getBlock moves 'd' diag)) && (2 == (length $ filter (\(Move pl x y) -> (player == pl)) (getBlock moves 'd' diag))))
                then (getFirstEmptyCell moves 0 0 (if diag == 0 then 'd' else 'b'))
                else generateWinnerDiagMove moves player (diag + 1)
    in
        cell

generateWinnerMove :: [Move] -> Char -> (Int, Int)
generateWinnerMove moves player = 
    let 
        cell =
            if ((-1, -1) /= (generateWinnerRowMove moves player 0))
                then (generateWinnerRowMove moves player 0)
                else if ((-1, -1) /= (generateWinnerColMove moves player 0))
                    then (generateWinnerColMove moves player 0)
                    else if ((-1, -1) /= (generateWinnerDiagMove moves player 0))
                        then (generateWinnerDiagMove moves player 0)
                        else (-1, -1)
    in
        cell

generateCell :: [Move] -> Char-> (Int, Int)
generateCell moves player =
    let
        cell = 
            if ((-1, -1) /= (generateWinnerMove moves player))
                then (generateWinnerMove moves player)
                else if ((-1, -1) /= (defend moves))
                    then (defend moves)
                    else if ((2 == length moves) && (2 == length (getOccupiedCorners moves)) && (getCellOfMove (moves !! 1) /= getOppositeCorner (moves !! 0)))
                        then (getOppositeCorner (moves !! 0))
                            else if ((isEmptyCell moves (1, 1)) && (1 < length (getOccupiedCorners moves)))
                            then (1, 1)
                            else if ((1 == length moves) && (isEmptyCell moves (1, 1)))
                                then (1, 1)
                                else if ((2 == length moves) && (isInCorner (moves !! 0)) && (isInMiddle (moves !! 1)))
                                    then getOppositeCorner (moves !! 0)
                                    else if ((3 == length moves) && (isInCorner (moves !! 0)) && (isInMiddle (moves !! 1)) && (isInOppositeCorner (moves !! 0) (moves !! 2)))
                                        then getFirstEmptySideCell moves
                                        else if (existsEmptyCorner moves)
                                            then getEmptyCorner moves
                                            else if isEmptyCell moves (1, 1)
                                                then (1,1)
                                                else if existsEmptyCell moves 0 0 't'
                                                    then getFirstEmptyCell moves 0 0 't'
                                                    else getFirstEmptyCell moves 3 3 'r'
    in
        cell

createMove :: Char -> [Move] -> Move
createMove player moves =
    let
        cell = generateCell moves player
        move = Move player (fst cell) (snd cell)
    in
        move

parse :: String -> [Move]
parse ('{':rest) = (parseMoves (trim rest))
parse _ = error "Ne sarasas"

parseMoves :: String -> [Move]
parseMoves "}" = []
parseMoves ('"' :key: '"' : ':' :rest) =
  let
    (move, restNow) = parseMove rest
    answ = parseMoves restNow
  in
    move:answ
parseMoves (',' :rest) = parseMoves rest
parseMoves str = error ("Invalid string: " ++ str)

parseMove :: String -> (Move, String)
parseMove ('{' : '"' : 'x' : '"' : ':' :x: ',' : '"' : 'y' : '"' : ':' :y: ',' : '"' : 'v' : '"' : ':' : '"' :v: '"' : '}' :rest) = ((Move v (digitToInt (x)) (digitToInt (y))), rest)
parseMove _ = error "Not valid move"

createMoveFromString :: String -> Move
createMoveFromString serialized = head (reverse (parse serialized))

getTurn :: [Move] -> Char
getTurn moves =
    let
        movesLength = length moves
        player = getPlayerOfMove (moves !! (movesLength - 1))

        turn = if movesLength > 0
            then
                if player == 'x'
                    then 'o'
                    else 'x'
            else 'x'

    in
        turn

isWinner :: [Move] -> Char -> Bool
isWinner moves player =
    let
        winner =
            (3 == (length $ filter (\(Move pl x y) -> (player == pl)) (getBlock moves 'r' 0))) ||
            (3 == (length $ filter (\(Move pl x y) -> (player == pl)) (getBlock moves 'r' 1))) ||
            (3 == (length $ filter (\(Move pl x y) -> (player == pl)) (getBlock moves 'r' 2))) ||
            (3 == (length $ filter (\(Move pl x y) -> (player == pl)) (getBlock moves 'c' 0))) ||
            (3 == (length $ filter (\(Move pl x y) -> (player == pl)) (getBlock moves 'c' 1))) ||
            (3 == (length $ filter (\(Move pl x y) -> (player == pl)) (getBlock moves 'c' 2))) ||
            (3 == (length $ filter (\(Move pl x y) -> (player == pl)) (getBlock moves 'd' 0))) ||
            (3 == (length $ filter (\(Move pl x y) -> (player == pl)) (getBlock moves 'd' 1)))
    in
        winner

getWinner :: [Move] -> Char
getWinner moves =
    let
        winner = if (isWinner moves 'x')
            then 'x'
            else if (isWinner moves 'o')
                then 'o'
                else if (9 == (length moves))
                    then 'e'
                    else '-'
    in
        winner

printWinner :: Char -> Char -> IO()
printWinner player winner = do
    if (player == winner)
        then print ("You won")
        else if (winner == '-')
            then print ("Match is not over yet")
            else if (winner == 'e')
                then print ("Draw!")
                else print ("You lose")

play :: Char -> [Move] -> [Char] -> IO()
play '*' moves myUrl = do print "Done"
play player moves myUrl = do
    if player == getTurn moves
        then do
            print "my turn"
            let move = createMove player moves
            let newMoves = addNewMove moves move

            (send newMoves myUrl) >>= (\ x -> if (x == 200) then do 
                    let winner = getWinner newMoves
                    if (winner /= '-')
                        then do
                            printTable newMoves
                            printWinner player winner
                        else (play player newMoves myUrl) 
                else do 
                    print x
                )
        else do
            print "not my turn. receiving opponent move"
            (receive myUrl) >>= (\ x -> if (isValidResponse (L8.unpack x)) then do
                    let move = createMoveFromString (trim (L8.unpack x))
                    let newMoves = addNewMove moves move

                    let winner = getWinner newMoves
                    if (winner /= '-')
                        then do
                            printTable newMoves
                            printWinner player winner
                        else (play player newMoves myUrl)
                else do
                    print (L8.unpack x)
                )

send :: [Move] -> [Char]-> IO Int
send moves postUrl = do
    let serializedTable = serializeTable moves

    manager <- newManager tlsManagerSettings
    initialRequest <- parseRequest postUrl
    let request = initialRequest
            { method = "POST"
            , requestBody = RequestBodyLBS $ L8.pack serializedTable
            , requestHeaders =
                [ 
                    ("Content-Type", "application/json+map")
                ]
            }

    response <- httpLbs request manager
    
    return (statusCode $ responseStatus response)

isValidResponse :: [Char] -> Bool
isValidResponse response =
    let
        isValid = if (((length response) > 0) && ((response !! 0) == '{'))
            then True
            else False
    in
        isValid

receive :: [Char] -> IO L8.ByteString
receive getUrl = do
    manager <- newManager tlsManagerSettings
    initialRequest <- parseRequest getUrl
    let request = initialRequest
            { method = "GET"
            , requestHeaders =
                [ 
                    ("Accept", "application/json+map")
                ]
            }

    response <- httpLbs request manager
    
    return (responseBody response)

main :: IO ()
main = do
    args <- getArgs
    let gameId = (args !! 0)
    let pl = if ((args !! 1) == "x") then "1" else "2"

    let myUrl = url ++ charArrToString gameId ++ "/player/" ++ charArrToString pl
    let moves = if (length args) == 3 then parse (args !! 2) else []

    play (if (pl == "1") then 'x' else 'o') moves myUrl