module Paths_antra (
    version,
    getBinDir, getLibDir, getDataDir, getLibexecDir,
    getDataFileName, getSysconfDir
  ) where

import qualified Control.Exception as Exception
import Data.Version (Version(..))
import System.Environment (getEnv)
import Prelude

catchIO :: IO a -> (Exception.IOException -> IO a) -> IO a
catchIO = Exception.catch

version :: Version
version = Version [0,1,0,0] []
bindir, libdir, datadir, libexecdir, sysconfdir :: FilePath

bindir     = "/home/benas/.cabal/bin"
libdir     = "/home/benas/.cabal/lib/x86_64-linux-ghc-7.10.3/antra-0.1.0.0-8E9vZjlfiFD3dsKaCnB8lO"
datadir    = "/home/benas/.cabal/share/x86_64-linux-ghc-7.10.3/antra-0.1.0.0"
libexecdir = "/home/benas/.cabal/libexec"
sysconfdir = "/home/benas/.cabal/etc"

getBinDir, getLibDir, getDataDir, getLibexecDir, getSysconfDir :: IO FilePath
getBinDir = catchIO (getEnv "antra_bindir") (\_ -> return bindir)
getLibDir = catchIO (getEnv "antra_libdir") (\_ -> return libdir)
getDataDir = catchIO (getEnv "antra_datadir") (\_ -> return datadir)
getLibexecDir = catchIO (getEnv "antra_libexecdir") (\_ -> return libexecdir)
getSysconfDir = catchIO (getEnv "antra_sysconfdir") (\_ -> return sysconfdir)

getDataFileName :: FilePath -> IO FilePath
getDataFileName name = do
  dir <- getDataDir
  return (dir ++ "/" ++ name)
