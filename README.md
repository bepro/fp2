paleidimas:

Sukompiliuot -> ghc Main

./Main gameId x <- puolantis zaidejas

./Main gameId o <- ginantis zaidejas

yra ir trecias neprivalomas parametras - pirmine zaidimo lenta (jei zaidziama ne nuo pradziu). duomenis reik paduot json be masyvu formatu

pvz 

./Main zdg o "{\"0\":  {\"x\":  0, \"y\": 1,  \"v\": \"x\"}, \"1\":  {\"x\": 2,   \"y\":   2,   \"v\":  \"o\"}, \"2\": {\"x\":  2,   \"y\":   0,  \"v\": \"x\"},   \"3\":   {\"x\":   2,  \"y\":  1,   \"v\": \"o\"}, \"4\": {\"x\":  1,   \"y\": 2,   \"v\":  \"x\"}}"